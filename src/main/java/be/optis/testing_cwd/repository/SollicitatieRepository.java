package be.optis.testing_cwd.repository;

import be.optis.testing_cwd.domain.Sollicitatie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SollicitatieRepository extends JpaRepository<Sollicitatie, Long> {

    List<Sollicitatie> findAllByCreator(String creator);
}
