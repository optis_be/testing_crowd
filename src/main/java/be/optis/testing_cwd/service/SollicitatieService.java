package be.optis.testing_cwd.service;

import be.optis.testing_cwd.domain.Sollicitatie;
import be.optis.testing_cwd.repository.SollicitatieRepository;
import be.optis.testing_cwd.util.SollicitatieComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SollicitatieService {

    private final SollicitatieRepository sollicitatieRepository;

    @Autowired
    public SollicitatieService(SollicitatieRepository sollicitatieRepository) {
        this.sollicitatieRepository = sollicitatieRepository;
    }

    public Sollicitatie createSollicitatie(Sollicitatie sollicitatie){
        sollicitatieRepository.save(sollicitatie);
        return sollicitatie;
    }

    public Sollicitatie getSollicitatie(Long id){
        return sollicitatieRepository.getOne(id);
    }

    public List<Sollicitatie> getSollicitatiesForCreator(String creator){
        List<Sollicitatie> sollicitaties = sollicitatieRepository.findAllByCreator(creator);
        sollicitaties.sort(new SollicitatieComparator());
        return sollicitaties;
    }
}
