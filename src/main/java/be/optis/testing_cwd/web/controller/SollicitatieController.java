package be.optis.testing_cwd.web.controller;

import be.optis.testing_cwd.domain.Sollicitatie;
import be.optis.testing_cwd.service.SollicitatieService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("sollicitaties")
@RestController
public class SollicitatieController {

    private final SollicitatieService sollicitatieService;

    public SollicitatieController(SollicitatieService sollicitatieService) {
        this.sollicitatieService = sollicitatieService;
    }

    @GetMapping
    public ResponseEntity<Sollicitatie> getRobot(@RequestParam Long id) {
        Sollicitatie sollicitatie = sollicitatieService.getSollicitatie(id);
        return ResponseEntity.ok(sollicitatie);
    }

    @GetMapping
    public ResponseEntity<List<Sollicitatie>> getSollicitatiesForUser(@RequestParam String creator) {
        return ResponseEntity.ok(sollicitatieService.getSollicitatiesForCreator(creator));
    }

}
