package be.optis.testing_cwd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestingCwdApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestingCwdApplication.class, args);
    }
}
