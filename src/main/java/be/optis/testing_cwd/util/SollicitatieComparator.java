package be.optis.testing_cwd.util;

import be.optis.testing_cwd.domain.Sollicitatie;

import java.util.Comparator;

public class SollicitatieComparator implements Comparator<Sollicitatie> {

    @Override
    public int compare(Sollicitatie o1, Sollicitatie o2) {
        if (o1 == o2) {
            return 0;
        }

        if (o1.isOpdracht() != o2.isOpdracht()) {
            return o1.isOpdracht() ? -1 : 1;
        }

        //In geval van beide == opdracht => vergelijk deadlines. Bij deadline == null staat die onderaan
        if (o1.isOpdracht()) {
            int deadlineCompare = o1.getDeadline().compareTo(o2.getDeadline());
            if (deadlineCompare != 0) {
                return deadlineCompare;
            }
        }

        return o1.compareTo(o2);
    }

}
