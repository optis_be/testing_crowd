package be.optis.testing_cwd.domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Access(AccessType.FIELD)
public class SollicitatieActiviteit implements Comparable<SollicitatieActiviteit> {

    @Id
    private Long id;

    private String creator;
    private LocalDateTime creationDateTime;
    private LocalDateTime updateDateTime;

//    @ManyToOne(optional = false)
//    private Sollicitatie sollicitatie;

    public SollicitatieActiviteit(SollicitatieActiviteitBuilder sollicitatieActiviteitBuilder) {
        this.creator = sollicitatieActiviteitBuilder.creator;
        this.creationDateTime = sollicitatieActiviteitBuilder.creationDateTime;
        this.updateDateTime = sollicitatieActiviteitBuilder.updateDateTime;
    }

    public SollicitatieActiviteit() {
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String jobseeker) {
        this.creator = jobseeker;
    }

    public LocalDateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(LocalDateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    @Override
    public int compareTo(SollicitatieActiviteit o) {
        if (this == o) {
            return 0;
        }
        if (o == null) {
            return -1;
        }

        if (this.updateDateTime == null && o.updateDateTime == null) {
            return this.creationDateTime.compareTo(o.creationDateTime);
        }

        if (this.updateDateTime == null) {
            return 1;
        }

        if (o.updateDateTime == null) {
            return -1;
        }

        if (this.updateDateTime.compareTo(o.updateDateTime) == 0) {
            return this.creationDateTime.compareTo(o.creationDateTime);
        } else {
            return this.updateDateTime.compareTo(o.updateDateTime);
        }
    }

    public static class SollicitatieActiviteitBuilder {
        private String creator;
        private LocalDateTime creationDateTime;
        private LocalDateTime updateDateTime;

        public SollicitatieActiviteit build() {
            return new SollicitatieActiviteit(this);
        }

        public SollicitatieActiviteitBuilder withCreator(String creator) {
            this.creator = creator;
            return this;
        }

        public SollicitatieActiviteitBuilder withCreationDateTime(LocalDateTime creationDateTime) {
            this.creationDateTime = creationDateTime;
            return this;
        }

        public SollicitatieActiviteitBuilder withUpdateDateTime(LocalDateTime updateDateTime) {
            this.updateDateTime = updateDateTime;
            return this;
        }
    }
}

