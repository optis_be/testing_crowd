package be.optis.testing_cwd.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@Entity
@Access(AccessType.FIELD)
public class Sollicitatie implements Comparable<Sollicitatie> {

    @Id
    private Long id;

    private String creator;
    private boolean isOpdracht;
    private LocalDate deadline;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @OrderBy("creationDateTime ASC")
    private SortedSet<SollicitatieActiviteit> activiteitenHistoriek;

    private Sollicitatie(SollicitatieBuilder sollicitatieBuilder) {
        this.creator = sollicitatieBuilder.creator;
        this.isOpdracht = sollicitatieBuilder.isOpdracht;
        this.deadline = sollicitatieBuilder.deadline;
        this.activiteitenHistoriek = sollicitatieBuilder.activiteitenHistoriek;
    }

    public Sollicitatie() {
    }

    public String getCreator() {
        return creator;
    }

    public boolean isOpdracht() {
        return isOpdracht;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public Set<SollicitatieActiviteit> getActiviteitenHistoriek() {
        return activiteitenHistoriek;
    }

    public SollicitatieActiviteit getLaatsteActiviteit() {
        return this.activiteitenHistoriek.last();
    }


    @Override
    public int compareTo(Sollicitatie o) {
        if (this == o) {
            return 0;
        }
        if (o == null) {
            return -1;
        }

        if (this.deadline != null && o.deadline != null) {
            if (this.deadline.compareTo(o.deadline) == 0) {
                return this.getLaatsteActiviteit().compareTo(o.getLaatsteActiviteit());
            }
            return this.deadline.compareTo(o.deadline);
        }

        if (this.deadline != null) {
            return 1;
        }
        if (o.deadline != null) {
            return -1;
        } else {
            return this.getLaatsteActiviteit().compareTo(o.getLaatsteActiviteit());
        }

    }

    public static class SollicitatieBuilder {

        private String creator;
        private boolean isOpdracht;
        private LocalDateTime updateTime;
        private LocalDate deadline;
        private final SortedSet<SollicitatieActiviteit> activiteitenHistoriek = new TreeSet<>();

        public Sollicitatie build() {
            Sollicitatie sollicitatie = new Sollicitatie(this);
            if (this.activiteitenHistoriek.isEmpty()) {
                throw new RuntimeException("ActiviteitenHistoriek cannot be empty");
            }
            return sollicitatie;
        }

        public SollicitatieBuilder withCreator(String creator) {
            this.creator = creator;
            return this;
        }

        public SollicitatieBuilder isOpdracht() {
            isOpdracht = true;
            return this;
        }

        public SollicitatieBuilder withDeadline(LocalDate deadline) {
            this.deadline = deadline;
            return this;
        }

        public SollicitatieBuilder withItem(SollicitatieActiviteit sollicitatieActiviteit) {
            this.activiteitenHistoriek.add(sollicitatieActiviteit);
            return this;
        }
    }
}

