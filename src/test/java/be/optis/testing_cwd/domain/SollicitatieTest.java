package be.optis.testing_cwd.domain;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;

public class SollicitatieTest {

    @Test
    public void sollicitatiesCreateTest() {
        LocalDateTime creationDate = LocalDateTime.of(2020, 2, 2, 2, 2);
        LocalDateTime updateDate = LocalDateTime.of(2020, 4, 2, 2, 2);
        LocalDate deadline = LocalDate.of(2020, 4, 2);
        Sollicitatie testSoll = new Sollicitatie.SollicitatieBuilder()
                .withCreator("Meneer")
                .withDeadline(deadline)
                .withItem(new SollicitatieActiviteit.SollicitatieActiviteitBuilder()
                        .withCreator("Meneer")
                        .withCreationDateTime(creationDate)
                        .withUpdateDateTime(creationDate)
                        .build())
                .withItem(new SollicitatieActiviteit.SollicitatieActiviteitBuilder()
                        .withCreator("Meneer")
                        .withCreationDateTime(updateDate)
                        .withUpdateDateTime(updateDate)
                        .build())
                .build();
        assertEquals("Meneer", testSoll.getCreator());
        assertEquals(deadline, testSoll.getDeadline());
    }


}
