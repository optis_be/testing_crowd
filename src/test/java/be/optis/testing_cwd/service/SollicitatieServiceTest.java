package be.optis.testing_cwd.service;

import be.optis.testing_cwd.domain.Sollicitatie;
import be.optis.testing_cwd.domain.SollicitatieActiviteit;
import be.optis.testing_cwd.repository.SollicitatieRepository;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SollicitatieServiceTest {

    @Mock
    SollicitatieRepository sollicitatieRepository;

    @InjectMocks
    SollicitatieService sollicitatieService;

    @Test
    public void sollicitatiesOrderTest() {
        when(sollicitatieRepository.findAllByCreator("Meneer")).thenReturn(getMockSollicitaties());

        List<Sollicitatie> sollicitaties = sollicitatieService.getSollicitatiesForCreator("Meneer");

        assertEquals(3, sollicitaties.size());
        assertEquals(2, sollicitaties.get(0).getActiviteitenHistoriek().size());
        assertEquals("Meneer 2", sollicitaties.get(1).getCreator());

    }

    private List<Sollicitatie> getMockSollicitaties() {
        return Lists.newArrayList(new Sollicitatie.SollicitatieBuilder()
                        .withCreator("Meneer 3")
                        .withItem(new SollicitatieActiviteit.SollicitatieActiviteitBuilder()
                                .withCreationDateTime(LocalDateTime.now().minusHours(7))
                                .build())
                        .withItem(new SollicitatieActiviteit.SollicitatieActiviteitBuilder()
                                .withCreationDateTime(LocalDateTime.now().minusHours(4))
                                .build())
                        .build(),

                new Sollicitatie.SollicitatieBuilder()
                        .withCreator("Meneer 1")
                        .withItem(new SollicitatieActiviteit.SollicitatieActiviteitBuilder()
                                .withCreationDateTime(LocalDateTime.now().minusHours(3))
                                .build())
                        .withItem(new SollicitatieActiviteit.SollicitatieActiviteitBuilder()
                                .withCreationDateTime(LocalDateTime.now().minusHours(2))
                                .build())
                        .build(),

                new Sollicitatie.SollicitatieBuilder()
                        .withCreator("Meneer 2")
                        .withItem(new SollicitatieActiviteit.SollicitatieActiviteitBuilder()
                                .withCreationDateTime(LocalDateTime.now().minusHours(4))
                                .build())
                        .withItem(new SollicitatieActiviteit.SollicitatieActiviteitBuilder()
                                .withCreationDateTime(LocalDateTime.now().minusHours(3))
                                .build())
                        .build()
        );
    }
}
